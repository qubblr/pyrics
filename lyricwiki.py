"""
Copyright 2009~2012 Bart Nagel (bart@tremby.net)

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

# -*- coding: utf-8 -*-
NAME = "lyrics"
VERSION = "2.0"
DESCRIPTION = """Python script making use of LyricWiki (lyrics.wikia.com) to pull
lyrics from the web from the commandline"""
AUTHOR = "Bart Nagel"
AUTHOR_EMAIL = "bart@tremby.net"
URL = "http://github.com/tremby/py-lyrics"
LICENSE = "Gnu GPL v3"

import urllib
import lxml.html

def lyricwikicase(s):
    """Return a string in LyricWiki case.
    Substitutions are performed as described at
    <http://lyrics.wikia.com/LyricWiki:Page_Names>.
    Essentially that means capitalizing every word and substituting certain
    characters."""

    words = s.split()
    newwords = []
    for word in words:
        newwords.append(word[0].capitalize() + word[1:])
    s = "_".join(newwords)
    s = s.replace("<", "Less_Than")
    s = s.replace(">", "Greater_Than")
    s = s.replace("#", "Number_") # FIXME: "Sharp" is also an allowed substitution
    s = s.replace("[", "(")
    s = s.replace("]", ")")
    s = s.replace("{", "(")
    s = s.replace("}", ")")

    s = s.encode('utf8')
    s = urllib.urlencode([(0, s)])[2:]
    return s

def lyricwikipagename(artist, title):
    """Return the page name for a set of lyrics given the artist and
    title"""

    return "%s:%s" % (lyricwikicase(artist), lyricwikicase(title))

def lyricwikiurl(artist, title, edit=False):
    """Return the URL of a LyricWiki page for the given song, or its edit
    page"""

    base = "http://lyrics.wikia.com/"
    pagename = lyricwikipagename(artist, title)
    if edit:
        return base + "index.php?title=%s&action=edit" % pagename
    return base + pagename

def search_artist(artist):
    """Return the name of a LyricWiki page for the given artist
    Return None if there is no artist"""

    base = "http://lyrics.wikia.com/"
    query = "index.php?search=%s&fulltext=Search" % artist.replace(' ', '+')
    try:
        doc = lxml.html.parse(''.join((base, query)))
    except IOError: raise
    try:
        artist_name = doc.xpath("//h1/a[@data-pos='1']")[0].text
        return artist_name
    except IndexError: raise

def getlyrics(artist, title):
    """Get and return the lyrics for the given song.
    Raises an IOError if the lyrics couldn't be found.
    Raises an IndexError if there is no lyrics tag.
    Returns None if there are no lyrics (it's instrumental)."""
    try:
        bad_artist = False
        doc = lxml.html.parse(lyricwikiurl(artist, title))
    except IOError:
        bad_artist = True
        pass # give it the second chance

    if bad_artist:
        try:
            artist = search_artist(artist)
            doc = lxml.html.parse(lyricwikiurl(artist, title))
        except IOError: raise

    try:
        lyricbox = doc.getroot().cssselect(".lyricbox")[0]
    except IndexError: raise

    # look for a sign that it's instrumental
    if len(doc.getroot().cssselect(".lyricbox a[title=\"Instrumental\"]")):
        return False

    # prepare output
    lyrics = []
    if lyricbox.text is not None:
        lyrics.append(lyricbox.text)
    for node in lyricbox:
        if str(node.tag).lower() == "br":
            lyrics.append("\n")
        if node.tail is not None:
            lyrics.append(node.tail)
    return "".join(lyrics).strip()
