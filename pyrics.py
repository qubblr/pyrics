#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Copyright 2013 Vladislav Kartashov (qubblr@gmail.com)
#
# Lyrics finder making use of LyricWiki (lyrics.wikia.com) to find and set
# lyrics to given .m4a files. Best for songs purchased in iTunes Store.


import os
import sys
from lyricwiki import getlyrics
from mutagen.mp4 import MP4


def get_title(path):
    """Return ID3 .m4a song title using Mutagen lib"""
    title = '\xa9nam'  # ID3 title tag key
    try:
        result = MP4(path).get(title).pop()
    except:
        return None
    return result


def get_artist(path):
    """Return ID3 .m4a song artist using Mutagen lib"""
    artist = '\xa9ART'
    try:
        result = MP4(path).get(artist).pop()
    except:
        return None
    return result


def get_album(path):
    """Return ID3 .m4a song album using Mutagen lib"""
    album = '\xa9alb'
    try:
        result = MP4(path).get(album).pop()
    except:
        return None
    return result


def set_lyrics(path, text):
    """Save ID3 .m4a song lyrics using Mutage lib"""
    lyric = '\xa9lyr'
    try:
        audio = MP4(path)
        audio[lyric] = text
        audio.save()
    except:
        return False
    return True


def tracklist(p):
    """Returns absolute path to *.m4a files.
    p can either be a dir path or a file path.
    Looks through all subdirs in case p is a dir path."""
    if os.path.splitext(p)[1] == '.m4a':  # only 1 file
        return [p]
    tlist = []
    for dirpath, _, filenames in os.walk(p):
        for f in filenames:
            if os.path.splitext(f)[1] == '.m4a':
                tlist.append(os.path.abspath(os.path.join(dirpath, f)))
    return tlist


def get_lyrics(track_path):
    """Get and return lyrics for .m4a file.
    Returns None if there is no such track."""
    artist = get_artist(track_path)
    title = get_title(track_path)
    try:
        lyrics = getlyrics(artist, title)
    except:
        return None
    return lyrics


def main():
    found_lyrics = 0
    path = sys.argv[1] if len(sys.argv) > 1 else '.'

    for t in tracklist(path):
        print os.path.basename(t)
        lyrics = get_lyrics(t)
        if lyrics:
            set_lyrics(t, lyrics)
            found_lyrics += 1

    tracks_count = len(tracklist(path))
    print "Found {0} of {1}".format(found_lyrics, tracks_count)

if __name__ == '__main__':
    main()
